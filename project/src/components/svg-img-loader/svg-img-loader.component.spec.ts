import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SvgImgLoaderComponent } from './svg-img-loader.component';

describe('SvgImgLoaderComponent', () => {
  let component: SvgImgLoaderComponent;
  let fixture: ComponentFixture<SvgImgLoaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SvgImgLoaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SvgImgLoaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
