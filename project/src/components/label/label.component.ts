import { Component, OnChanges, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-label',
  templateUrl: './label.component.html',
  styleUrls: ['./label.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LabelComponent implements OnChanges {

  /* @Input() title: string;
  @Input() value: string;
  @Input() desc: string;
  @Input() titleVariation: string = 'small-weak-UI-6';
  @Input() valueVariation: string = 'base-strong-UI-7';
  @Input() descVariation: string = 'small-weak-UI-6'; */

  constructor() { }

  ngOnChanges() {
  }

}
