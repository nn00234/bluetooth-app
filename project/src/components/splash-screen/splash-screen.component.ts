import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-splash-screen',
  templateUrl: './splash-screen.component.html',
  styleUrls: ['./splash-screen.component.scss']
})
export class SplashScreenComponent implements OnInit {

  public windowWidth: string;
  public showSplashScreen: boolean = true;

  constructor() { }

  ngOnInit(): void {
    setTimeout(() => {
      this.windowWidth = `-${window.innerWidth}px`
      setTimeout(() => this.showSplashScreen = false, 3000);
    }, 3000);
  }

}
