import { Component, OnInit } from '@angular/core';
import { CommonService } from '../service/common.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  public showLoader = true;
  public dashTitle: string = 'length counter'.toUpperCase();
  public dataArray = [
    {
      id: 'test1',
      title: 'Title 1',
      value: '15 rpm',
      desc: 'This the test case 1'
    },
    {
      id: 'test2',
      title: 'Title 2',
      value: '10 rpm',
      desc: 'This the test case 2'
    },
    {
      id: 'test3',
      title: 'Title 3',
      value: '100 rpm',
      desc: 'This the test case 2'
    }
  ];

  constructor(
    private commonService: CommonService
  ) { }

  ngOnInit() {
    const pageName = this.commonService.getValue<string>('Dashboard_route');
    if (pageName) {
      this.dashTitle = pageName.toUpperCase();
    }
    setTimeout(() => this.showLoader = false, 3000);
  }

}
