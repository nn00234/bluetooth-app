import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { MatBottomSheetModule } from '@angular/material';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { SvgImgLoaderComponent } from 'project/src/components/svg-img-loader/svg-img-loader.component';
import { SplashScreenComponent } from 'project/src/components/splash-screen/splash-screen.component';
import { LoaderComponent } from 'project/src/components/loader/loader.component';
import { ButtonComponent } from 'project/src/components/button/button.component';
// import { DashboardComponent } from 'project/src/dashboard/dashboard.component';
import { LabelComponent } from 'project/src/components/label/label.component';
import { HomeComponent } from 'project/src/home/home.component';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SplashScreenComponent,
    // DashboardComponent,
    SvgImgLoaderComponent,
    LabelComponent,
    LoaderComponent,
    ButtonComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatBottomSheetModule,
    HttpClientModule
  ],
  entryComponents: [],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
